[x-cube-app-sdk](../README.md) / [Exports](../modules.md) / Button

# Enumeration: Button

## Table of contents

### Enumeration members

- [BLUE](Button.md#blue)
- [GREEN](Button.md#green)
- [RED](Button.md#red)
- [WHITE](Button.md#white)
- [YELLOW](Button.md#yellow)

## Enumeration members

### BLUE

• **BLUE** = `5`

___

### GREEN

• **GREEN** = `4`

___

### RED

• **RED** = `2`

___

### WHITE

• **WHITE** = `3`

___

### YELLOW

• **YELLOW** = `1`
