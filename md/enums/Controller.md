[x-cube-app-sdk](../README.md) / [Exports](../modules.md) / Controller

# Enumeration: Controller

## Table of contents

### Enumeration members

- [FRONT\_LEFT](Controller.md#front_left)
- [FRONT\_RIGHT](Controller.md#front_right)
- [LEFT](Controller.md#left)
- [REAR\_LEFT](Controller.md#rear_left)
- [REAR\_RIGHT](Controller.md#rear_right)
- [RIGHT](Controller.md#right)

## Enumeration members

### FRONT\_LEFT

• **FRONT\_LEFT** = `6`

___

### FRONT\_RIGHT

• **FRONT\_RIGHT** = `1`

___

### LEFT

• **LEFT** = `5`

___

### REAR\_LEFT

• **REAR\_LEFT** = `4`

___

### REAR\_RIGHT

• **REAR\_RIGHT** = `3`

___

### RIGHT

• **RIGHT** = `2`
