[x-cube-app-sdk](../README.md) / [Exports](../modules.md) / IHostApi

# Interface: IHostApi

## Table of contents

### Methods

- [blackLightOff](IHostApi.md#blacklightoff)
- [blackLightOn](IHostApi.md#blacklighton)
- [controllerEpaperOn](IHostApi.md#controllerepaperon)
- [controllerLedStrip](IHostApi.md#controllerledstrip)
- [controllerLedsBlink](IHostApi.md#controllerledsblink)
- [controllerLedsOff](IHostApi.md#controllerledsoff)
- [controllerLedsOn](IHostApi.md#controllerledson)
- [controllerTouchOn](IHostApi.md#controllertouchon)
- [controllerVuOn](IHostApi.md#controllervuon)
- [lightsOff](IHostApi.md#lightsoff)
- [lightsOn](IHostApi.md#lightson)
- [photoOff](IHostApi.md#photooff)
- [photoOn](IHostApi.md#photoon)
- [resetControllers](IHostApi.md#resetcontrollers)
- [strobeOff](IHostApi.md#strobeoff)
- [strobeOn](IHostApi.md#strobeon)

## Methods

### blackLightOff

▸ **blackLightOff**(): `Promise`<`void`\>

Turn the black light off

#### Returns

`Promise`<`void`\>

___

### blackLightOn

▸ **blackLightOn**(): `Promise`<`void`\>

Turn the black light on

#### Returns

`Promise`<`void`\>

___

### controllerEpaperOn

▸ **controllerEpaperOn**(`controllers`, `epaperOn`): `Promise`<`void`\>

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `controllers` | [`Controller`](../enums/Controller.md)[] | Array of controllers to send the message to |
| `epaperOn` | `boolean` | Boolean to toggle the epaper on or off |

#### Returns

`Promise`<`void`\>

___

### controllerLedStrip

▸ **controllerLedStrip**(`controllers`, `color`, `brightness`, `blink`): `Promise`<`void`\>

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `controllers` | [`Controller`](../enums/Controller.md)[] | Array of controllers to send the message to |
| `color` | `Object` | RGB color to set the led strip to |
| `color.b` | `number` | - |
| `color.g` | `number` | - |
| `color.r` | `number` | - |
| `brightness` | `number` | Brightness of the led strip (1-255) |
| `blink` | `number` | - |

#### Returns

`Promise`<`void`\>

___

### controllerLedsBlink

▸ **controllerLedsBlink**(`controllers`, `leds`, `interval`): `Promise`<`void`\>

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `controllers` | [`Controller`](../enums/Controller.md)[] | Array of controllers to send the message to |
| `leds` | [`Button`](../enums/Button.md)[] | Array of numbers (1-5) of leds to blink |
| `interval` | `number`[] | Array of numbers that represent the interval of the blink for each led |

#### Returns

`Promise`<`void`\>

___

### controllerLedsOff

▸ **controllerLedsOff**(`controllers`, `ledsOff`): `Promise`<`void`\>

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `controllers` | [`Controller`](../enums/Controller.md)[] | Array of controllers to send the message to |
| `ledsOff` | [`Button`](../enums/Button.md)[] | Array of numbers (1-5) of leds to turn off |

#### Returns

`Promise`<`void`\>

___

### controllerLedsOn

▸ **controllerLedsOn**(`controllers`, `ledsOn`): `Promise`<`void`\>

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `controllers` | [`Controller`](../enums/Controller.md)[] | Array of controllers to send the message to |
| `ledsOn` | [`Button`](../enums/Button.md)[] | Array of numbers (1-5) of leds to turn on |

#### Returns

`Promise`<`void`\>

___

### controllerTouchOn

▸ **controllerTouchOn**(`controllers`, `touchOn`): `Promise`<`void`\>

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `controllers` | [`Controller`](../enums/Controller.md)[] | Array of controllers to send the message to |
| `touchOn` | `boolean` | Boolean to toggle the touch on or off |

#### Returns

`Promise`<`void`\>

___

### controllerVuOn

▸ **controllerVuOn**(`controllers`, `vuOn`): `Promise`<`void`\>

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `controllers` | [`Controller`](../enums/Controller.md)[] | Array of controllers to send the message to |
| `vuOn` | `boolean` | Boolean to toggle the vu on or off |

#### Returns

`Promise`<`void`\>

___

### lightsOff

▸ **lightsOff**(): `Promise`<`void`\>

Turn the regular light off

#### Returns

`Promise`<`void`\>

___

### lightsOn

▸ **lightsOn**(): `Promise`<`void`\>

Turn the regular light on

#### Returns

`Promise`<`void`\>

___

### photoOff

▸ **photoOff**(): `Promise`<`void`\>

Deactivate the photo scene

#### Returns

`Promise`<`void`\>

___

### photoOn

▸ **photoOn**(): `Promise`<`void`\>

Activate the photo scene

#### Returns

`Promise`<`void`\>

___

### resetControllers

▸ **resetControllers**(): `Promise`<`void`\>

Reset the controllers

#### Returns

`Promise`<`void`\>

___

### strobeOff

▸ **strobeOff**(): `Promise`<`void`\>

Turn the strobe light off

#### Returns

`Promise`<`void`\>

___

### strobeOn

▸ **strobeOn**(): `Promise`<`void`\>

Turn the strobe light on

#### Returns

`Promise`<`void`\>
