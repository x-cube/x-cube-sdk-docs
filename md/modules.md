[x-cube-app-sdk](README.md) / Exports

# x-cube-app-sdk

## Table of contents

### Enumerations

- [Button](enums/Button.md)
- [Controller](enums/Controller.md)

### Interfaces

- [IHostApi](interfaces/IHostApi.md)

### Variables

- [publicApi](modules.md#publicapi)

## Variables

### publicApi

• **publicApi**: [`IHostApi`](interfaces/IHostApi.md)
